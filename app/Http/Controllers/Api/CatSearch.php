<?php

namespace App\Http\Controllers\Api;
use App\Category;
use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;

class CatSearch extends Controller
{
    public function category_search(Request $request)
    {
        $data = $request->get('data');
        $search_categories = Category::where('name', 'like', "%{$data}%")->paginate(2);
        return Response::json([
            'data' => $search_categories
        ]);
    }

    public function most_viewed(){
        $categories=Category::whereHas('views')->get();

        return Response::json([
            'data' => $categories
        ]);
    }

    public function views_record(){

    }
}
