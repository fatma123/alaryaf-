<?php

namespace App\Http\Controllers\Api;
use App\Certifications;
use App\Http\Resources\UserResource;
use App\Http\Traits\ApiResponses;
use App\Http\Traits\UserOperation;
use App\User;
use Hash;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{

    use ApiResponses,UserOperation;
//    public function __construct()
//    {
//        $this->middleware('auth:api', ['except' => ['login']]);
//
//    }


    public function register(Request $request)
    {
        $rules=[
            'name' =>'required|string|max:191',
            'email'      =>'required|email:max:191|unique:users',
            'phone'      =>'required|string|max:191|unique:users',
            'password'   =>'required|string|max:191',
        ];
        $validate = Validator::make(request()->all(),$rules);

        if($validate->fails())
        {
            return response(['status'=>false,'messages'=>$validate->messages()]);
        }
        $inputs=$request->all();
        $inputs['password']=Hash::make($request->password);

       $user= User::create($inputs);

        if ($user->save()) {
            $user->login = [
                'href' => 'api/auth/login',
                'method' => 'POST',
                'params' => 'email, password'
            ];
            $response = [
                'msg' => 'User created',
                'user' => $user
            ];
            return response()->json($response, 201);
        }

        $response = [
            'msg' => 'An error occurred'
        ];

        return response()->json($response, 404);

    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $credentials = $request->only('email', 'password');


        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['msg' => 'Invalid credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['msg' => 'Could not create token'], 500);
        }

        return response()->json(['token' => $token]);
    }

    public function UpdateProfile(Request $request)
    {
        $user = auth('api')->user();
//        dd($user);
        $rules = [
            'name' =>'sometimes|string|max:191',
            'email' => 'sometimes|string|email|max:255|unique:users,email,' . $user->id,
            'password'   =>'string|max:191',
            'phone' =>'sometimes|string'
        ];
        $validation=$this->apiValidation($request,$rules);
        if($validation instanceof Response){return $validation;}
        $this->UpdateUser($user,$request);
        if ($user)  return $this->apiResponse(new UserResource($user),null,200);
        return $this->unKnowError();
//        auth('api');

    }






}
