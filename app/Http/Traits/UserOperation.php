<?php


namespace App\Http\Traits;


use App\Address;
use App\Certifications;
use App\Lawyer;
use App\User;
use Hash;
use Illuminate\Http\Request;

trait UserOperation
{
   public function RegisterUser($request,$role)
  {
      $inputs = $request->all();
      if ($request->image != null)
      {
          if ($request->hasFile('image')) {
              $picture = uploader($request,'image');
              $inputs['image'] = $picture;
          }
      }

      $inputs['socials']=json_encode($request['socials']);
      $inputs['password']=Hash::make($request->password);
      $inputs['role'] = $role;
      $inputs['confirmation_code']=12345;
      $user=User::create($inputs);
      $inputs['user_id']=$user->id;
      if ($role=='lawyer'){
          Lawyer::create(['user_id'=>$user->id]);
         if ($request->hasFile('certifications')){
             multiUploader($request,'certifications',new Certifications,['user_id'=>$user->id]);
         }
         if ($request->has('services')){
             $user->services()->syncWithoutDetaching($request['services']);
         }
      }
      return $user;
  }
    public function UpdateUser($user,$request)
    {
        $inputs = $request->all();
        if ($request->image != null)
        {
            if ($request->hasFile('image')) {
                $profileimage = uploader($request,'image');
                $user->update(['image' => $profileimage]);
            }
        }

        if(isset($inputs['full_phone'])){$inputs['phone'] = $inputs['full_phone'];}
        if($request->password != null) {$user->update(['password'=>bcrypt($request->password),]);}
        $user->update(array_except($inputs,['image','password']));
    }

    public function UpdateClientProfile($user,$request)
    {
        $inputs = $request->all();

        if ($request->image != null)
        {
            if ($request->hasFile('image')) {
                $picture = uploader($request,'image');
                $user->update(['image' => $picture]);
            }
        }
        if ($user->role=='lawyer'){
            $user->lawyer->update($request->all());
        }
        if($request->password != null) {$user->update(['password'=>Hash::make($request->password)]);}

        return $user->update(array_except($inputs,['password','image']));
    }

    public function UpdateClientSetting($user,$request)
    {
        $inputs = $request->all();
        return $user->update($inputs);
    }

    public function UpdateClientLocation($user,$request)
    {
        $inputs = $request->all();
        return $user->update($request->only('lat','long'));
    }

    public function UpdateUserAddress($user,$request)
    {
        $inputs = $request->all();
        $inputs['user_id'] = $user->id;
        return Address::create($inputs);
    }
}