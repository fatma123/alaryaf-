<?php


namespace App\Http\Traits;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

trait ApiResponses
{
    public $paginateNumber = 10;


    public function apiResponse($data = null , $error = null ,$code = 200){

        if ($error != null)
        {
            $array = [
                'status' => $code,
                'msg' => $error,
            ];
        }
        else
        {
            $array = [
                'status' => $code,
                'data' => $data,
            ];
        }
        return response($array , $code);
    }

    public function successCode(){
        return [
            200 , 201 , 202
        ];
    }

    public function createdResponse($data){
        return $this->apiResponse($data, null, 201);
    }

    public function deleteResponse(){
        return $this->apiResponse(true, null, 200);
    }

    public function notFoundResponse(){
        return $this->apiResponse(null, __('messages.not_found'), 404);
    }
    public function unAuthorized(){
        return $this->apiResponse(null, 'user is not unAuthorized !', 419);
    }

    public function unKnowError(){
        return $this->apiResponse(null, 'Un know error', 520);
    }

    public function apiValidation($request, $array){

        $validate = Validator::make($request->all() , $array);

        $errors = [];

        if ($validate->fails()) {
            return $this->apiResponse(null, $validate->getMessageBag()->first(), 422);
        }

    }

    /**
     * Collection Paginate
     *
     * @param $items
     * @param null $page
     * @param array $options
     * @return LengthAwarePaginator
     */
    public function CollectionPaginate($items, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $this->paginateNumber), $items->count(), $this->paginateNumber, $page, $options);
    }


}