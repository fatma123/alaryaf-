<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrdersCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'orders'=>$this->collection->transform(function ($q){
                return[
                    'id'=>$q->id,
                    'name'=>$q->name,
                    'description'=>$q->description,
                    'status'=>$q->status,
                    'user_id'=>$q->user_id,
                    'user_name'=>$q->user->name,
                    'user_image'=>getimg($q->user->image),
                    'service_id'=>$q->service_id,
                    'service_name'=>$q->service->name,
                    'service_description'=>$q->service->description,
                    'service_price'=>$q->service->price,
                    'service_image'=>getimg($q->Service->image),
                    'created_at'=>$q->created_at->format('Y-m-d H:i'),
                    'files'=>$q->files->transform(function ($file){return getimg($file->file);}),
                    'refers'=> $q->refers->transform(function ($user){
                        return[
                            'name'=>$user->name,
                            'id'=>$user->id,
                            'status'=>$user->status,
                            'image'=>getimg($user->image)
                        ];
                    })
                ];
            }),
            'paginate'=>[
                'total' => $this->total(),
                'count' => $this->count(),
                'per_page' => $this->perPage(),
                'next_page_url'=>$this->nextPageUrl(),
                'prev_page_url'=>$this->previousPageUrl(),
                'current_page' => $this->currentPage(),
                'total_pages' => $this->lastPage()
            ]
        ];
    }
    public function withResponse($request, $response)
    {
        $originalContent = $response->getOriginalContent();
        unset($originalContent['links'],$originalContent['meta']);
        $response->setData($originalContent);
    }
}
