<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class lawyerSingle extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'specialization'=>$this->specialization,
            'major'=>$this->major,
            'graduation_year'=>$this->graduation_year,
            'grade'=>$this->grade,
            'degree'=>$this->degree,
            'university'=>$this->university,
            'current_jop'=>$this->current_jop,
            'year_exp'=>$this->year_exp,
        ];
    }
}
