<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id'=>$this->id,
            'name'=>$this->name,
            'description'=>$this->description,
            'status'=>$this->status,
            'user_id'=>$this->user_id,
            'user_name'=>$this->user->name,
            'user_image'=>getimg($this->user->image),
            'service_id'=>$this->service_id,
            'service_name'=>$this->service->name,
            'created_at'=>$this->created_at->format('Y-m-d H:i'),
            'files'=>$this->files->transform(function ($q){return getimg($q->file);}),
            'refers'=> UserResource::collection($this->refers)

        ];
    }
}
