<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 'image'
    ];
    public function views()
    {
        return $this->hasMany(View::class,'category_id');
    }

}
