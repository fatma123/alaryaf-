<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    protected $fillable = [
        'category_id', 'user_id'
    ];

    public function categories()
    {
        $this->belongsTo(Category::class);
    }
    public function users()
    {
        $this->belongsTo(User::class);
    }
}
