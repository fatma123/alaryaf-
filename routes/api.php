<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'api','prefix' => 'auth'], function ($router) {
    Route::get('logout', 'Api\AuthController@logout');
    Route::Patch('profile','Api\AuthController@UpdateProfile');
});

Route::Post('register', 'Api\AuthController@register');
Route::post('login', 'Api\AuthController@login');
Route::resource('categories', 'Api\CategoriesController');
Route::get('results','Api\CatSearch@category_search');
Route::get('most_viewed','Api\CatSearch@most_viewed');
Route::get('views_record','Api\CatSearch@');